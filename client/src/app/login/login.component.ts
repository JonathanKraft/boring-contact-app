import { Component, OnInit } from '@angular/core';
import { User } from "../entity/user";
import { RouterExtensions } from "nativescript-angular/router";
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'ns-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  moduleId: module.id,
})
export class LoginComponent implements OnInit {

  user:User = {email:'', password:''};
  confirm = '';
  signup = false;
  feedback = '';
  
  constructor(private authService:AuthenticationService, 
    private routerExt:RouterExtensions) { }


  ngOnInit() {
  }

  register() {    
    if(this.user.password === this.confirm) {
      
      this.authService.addUser(this.user)
        .subscribe(
          user => this.login(),
          error => this.feedback = 'Register error'
        );
    } else {
      this.feedback = 'Password did not match.'
    }
    
  }

  login() {
    this.authService.login(this.user.email, this.user.password)
    .subscribe(
      () => {/*this.routerExt.backToPreviousPage(),*/ this.feedback = 'Congratz';
    console.log('Gratz');
    },
      (err) => {this.feedback = 'Login error.'; console.log(err)}
      
    );
  }

}
