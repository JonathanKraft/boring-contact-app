"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var operators_1 = require("rxjs/operators");
var environment_1 = require("../../environments/environment");
var appSetting = require("application-settings");
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http) {
        this.http = http;
        this.apiUrl = environment_1.environment.serverUrl;
    }
    AuthenticationService.prototype.addUser = function (user) {
        return this.http.post(this.apiUrl + '/user', user);
    };
    AuthenticationService.prototype.login = function (email, password) {
        return this.http.post(this.apiUrl + '/login_check', {
            username: email,
            password: password
        }).pipe(operators_1.tap(function (data) {
            if (data.token) {
                appSetting.setString('token', data.token);
            }
        }));
    };
    AuthenticationService.prototype.logout = function () {
        appSetting.remove('token');
    };
    AuthenticationService.prototype.isLogged = function () {
        return appSetting.hasKey('token');
    };
    AuthenticationService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImF1dGhlbnRpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFDM0MsNkNBQWtEO0FBR2xELDRDQUFxQztBQUNyQyw4REFBMkQ7QUFFM0QsaURBQW1EO0FBT25EO0lBR0UsK0JBQW9CLElBQWU7UUFBZixTQUFJLEdBQUosSUFBSSxDQUFXO1FBRjNCLFdBQU0sR0FBRyx5QkFBVyxDQUFDLFNBQVMsQ0FBQztJQUVBLENBQUM7SUFFeEMsdUNBQU8sR0FBUCxVQUFRLElBQVM7UUFDZixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQU8sSUFBSSxDQUFDLE1BQU0sR0FBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUQsQ0FBQztJQUVELHFDQUFLLEdBQUwsVUFBTSxLQUFZLEVBQUUsUUFBZTtRQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQU0sSUFBSSxDQUFDLE1BQU0sR0FBRyxjQUFjLEVBQUU7WUFDdkQsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsUUFBUTtTQUNuQixDQUFDLENBQUMsSUFBSSxDQUNMLGVBQUcsQ0FBQyxVQUFBLElBQUk7WUFDTixFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDZCxVQUFVLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUMsQ0FBQztRQUNILENBQUMsQ0FBQyxDQUNILENBQUM7SUFDSixDQUFDO0lBRUQsc0NBQU0sR0FBTjtRQUNFLFVBQVUsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVELHdDQUFRLEdBQVI7UUFDRSxNQUFNLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBNUJVLHFCQUFxQjtRQUhqQyxpQkFBVSxDQUFDO1lBQ1YsVUFBVSxFQUFFLE1BQU07U0FDbkIsQ0FBQzt5Q0FJeUIsaUJBQVU7T0FIeEIscUJBQXFCLENBNkJqQztJQUFELDRCQUFDO0NBQUEsQUE3QkQsSUE2QkM7QUE3Qlksc0RBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IFVzZXIgfSBmcm9tICcuLi9lbnRpdHkvdXNlcic7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YXAgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcbmltcG9ydCB7ZW52aXJvbm1lbnR9IGZyb20gJy4uLy4uL2Vudmlyb25tZW50cy9lbnZpcm9ubWVudCc7XG5cbmltcG9ydCAqIGFzIGFwcFNldHRpbmcgZnJvbSAnYXBwbGljYXRpb24tc2V0dGluZ3MnO1xuXG5cblxuQEluamVjdGFibGUoe1xuICBwcm92aWRlZEluOiAncm9vdCdcbn0pXG5leHBvcnQgY2xhc3MgQXV0aGVudGljYXRpb25TZXJ2aWNlIHtcbiAgcHJpdmF0ZSBhcGlVcmwgPSBlbnZpcm9ubWVudC5zZXJ2ZXJVcmw7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOkh0dHBDbGllbnQpIHsgfVxuXG4gIGFkZFVzZXIodXNlcjpVc2VyKTogT2JzZXJ2YWJsZTxVc2VyPiB7XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0PFVzZXI+KHRoaXMuYXBpVXJsKyAnL3VzZXInLCB1c2VyKTtcbiAgfVxuXG4gIGxvZ2luKGVtYWlsOnN0cmluZywgcGFzc3dvcmQ6c3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Q8YW55Pih0aGlzLmFwaVVybCArICcvbG9naW5fY2hlY2snLCB7XG4gICAgICB1c2VybmFtZTogZW1haWwsXG4gICAgICBwYXNzd29yZDogcGFzc3dvcmRcbiAgICB9KS5waXBlKFxuICAgICAgdGFwKGRhdGEgPT4ge1xuICAgICAgICBpZihkYXRhLnRva2VuKSB7XG4gICAgICAgICAgYXBwU2V0dGluZy5zZXRTdHJpbmcoJ3Rva2VuJywgZGF0YS50b2tlbik7XG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgKTtcbiAgfVxuICBcbiAgbG9nb3V0KCkge1xuICAgIGFwcFNldHRpbmcucmVtb3ZlKCd0b2tlbicpO1xuICB9XG5cbiAgaXNMb2dnZWQoKSB7XG4gICAgcmV0dXJuIGFwcFNldHRpbmcuaGFzS2V5KCd0b2tlbicpO1xuICB9XG59XG4iXX0=