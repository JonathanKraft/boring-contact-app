import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../entity/user';
import { Observable } from 'rxjs';
import { tap } from "rxjs/operators";
import {environment} from '../../environments/environment';

import * as appSetting from 'application-settings';



@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private apiUrl = environment.serverUrl;

  constructor(private http:HttpClient) { }

  addUser(user:User): Observable<User> {
    return this.http.post<User>(this.apiUrl+ '/user', user);
  }

  login(email:string, password:string): Observable<any> {
    return this.http.post<any>(this.apiUrl + '/login_check', {
      username: email,
      password: password
    }).pipe(
      tap(data => {
        if(data.token) {
          appSetting.setString('token', data.token);
        }
      })
    );
  }
  
  logout() {
    appSetting.remove('token');
  }

  isLogged() {
    return appSetting.hasKey('token');
  }
}
