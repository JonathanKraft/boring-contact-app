"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var authentication_service_1 = require("../service/authentication.service");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, routerExt) {
        this.authService = authService;
        this.routerExt = routerExt;
        this.user = { email: '', password: '' };
        this.confirm = '';
        this.signup = false;
        this.feedback = '';
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.register = function () {
        var _this = this;
        if (this.user.password === this.confirm) {
            this.authService.addUser(this.user)
                .subscribe(function (user) { return _this.login(); }, function (error) { return _this.feedback = 'Register error'; });
        }
        else {
            this.feedback = 'Password did not match.';
        }
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.authService.login(this.user.email, this.user.password)
            .subscribe(function () {
            _this.feedback = 'Congratz';
            console.log('Gratz');
        }, function (err) { _this.feedback = 'Login error.'; console.log(err); });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'ns-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css'],
            moduleId: module.id,
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
            router_1.RouterExtensions])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQWtEO0FBRWxELHNEQUErRDtBQUMvRCw0RUFBMEU7QUFRMUU7SUFPRSx3QkFBb0IsV0FBaUMsRUFDM0MsU0FBMEI7UUFEaEIsZ0JBQVcsR0FBWCxXQUFXLENBQXNCO1FBQzNDLGNBQVMsR0FBVCxTQUFTLENBQWlCO1FBTnBDLFNBQUksR0FBUSxFQUFDLEtBQUssRUFBQyxFQUFFLEVBQUUsUUFBUSxFQUFDLEVBQUUsRUFBQyxDQUFDO1FBQ3BDLFlBQU8sR0FBRyxFQUFFLENBQUM7UUFDYixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ2YsYUFBUSxHQUFHLEVBQUUsQ0FBQztJQUcwQixDQUFDO0lBR3pDLGlDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsaUNBQVEsR0FBUjtRQUFBLGlCQVlDO1FBWEMsRUFBRSxDQUFBLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7WUFFdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztpQkFDaEMsU0FBUyxDQUNSLFVBQUEsSUFBSSxJQUFJLE9BQUEsS0FBSSxDQUFDLEtBQUssRUFBRSxFQUFaLENBQVksRUFDcEIsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsUUFBUSxHQUFHLGdCQUFnQixFQUFoQyxDQUFnQyxDQUMxQyxDQUFDO1FBQ04sQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ04sSUFBSSxDQUFDLFFBQVEsR0FBRyx5QkFBeUIsQ0FBQTtRQUMzQyxDQUFDO0lBRUgsQ0FBQztJQUVELDhCQUFLLEdBQUw7UUFBQSxpQkFTQztRQVJDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2FBQzFELFNBQVMsQ0FDUjtZQUFnRCxLQUFJLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztZQUM3RSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JCLENBQUMsRUFDQyxVQUFDLEdBQUcsSUFBTSxLQUFJLENBQUMsUUFBUSxHQUFHLGNBQWMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUEsQ0FBQSxDQUFDLENBRTVELENBQUM7SUFDSixDQUFDO0lBckNVLGNBQWM7UUFOMUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFdBQVcsRUFBRSx3QkFBd0I7WUFDckMsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7WUFDcEMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1NBQ3BCLENBQUM7eUNBUWdDLDhDQUFxQjtZQUNqQyx5QkFBZ0I7T0FSekIsY0FBYyxDQXVDMUI7SUFBRCxxQkFBQztDQUFBLEFBdkNELElBdUNDO0FBdkNZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFVzZXIgfSBmcm9tIFwiLi4vZW50aXR5L3VzZXJcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICducy1sb2dpbicsXG4gIHRlbXBsYXRlVXJsOiAnLi9sb2dpbi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2xvZ2luLmNvbXBvbmVudC5jc3MnXSxcbiAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcbn0pXG5leHBvcnQgY2xhc3MgTG9naW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIHVzZXI6VXNlciA9IHtlbWFpbDonJywgcGFzc3dvcmQ6Jyd9O1xuICBjb25maXJtID0gJyc7XG4gIHNpZ251cCA9IGZhbHNlO1xuICBmZWVkYmFjayA9ICcnO1xuICBcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBhdXRoU2VydmljZTpBdXRoZW50aWNhdGlvblNlcnZpY2UsIFxuICAgIHByaXZhdGUgcm91dGVyRXh0OlJvdXRlckV4dGVuc2lvbnMpIHsgfVxuXG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICByZWdpc3RlcigpIHsgICAgXG4gICAgaWYodGhpcy51c2VyLnBhc3N3b3JkID09PSB0aGlzLmNvbmZpcm0pIHtcbiAgICAgIFxuICAgICAgdGhpcy5hdXRoU2VydmljZS5hZGRVc2VyKHRoaXMudXNlcilcbiAgICAgICAgLnN1YnNjcmliZShcbiAgICAgICAgICB1c2VyID0+IHRoaXMubG9naW4oKSxcbiAgICAgICAgICBlcnJvciA9PiB0aGlzLmZlZWRiYWNrID0gJ1JlZ2lzdGVyIGVycm9yJ1xuICAgICAgICApO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmZlZWRiYWNrID0gJ1Bhc3N3b3JkIGRpZCBub3QgbWF0Y2guJ1xuICAgIH1cbiAgICBcbiAgfVxuXG4gIGxvZ2luKCkge1xuICAgIHRoaXMuYXV0aFNlcnZpY2UubG9naW4odGhpcy51c2VyLmVtYWlsLCB0aGlzLnVzZXIucGFzc3dvcmQpXG4gICAgLnN1YnNjcmliZShcbiAgICAgICgpID0+IHsvKnRoaXMucm91dGVyRXh0LmJhY2tUb1ByZXZpb3VzUGFnZSgpLCovIHRoaXMuZmVlZGJhY2sgPSAnQ29uZ3JhdHonO1xuICAgIGNvbnNvbGUubG9nKCdHcmF0eicpO1xuICAgIH0sXG4gICAgICAoZXJyKSA9PiB7dGhpcy5mZWVkYmFjayA9ICdMb2dpbiBlcnJvci4nOyBjb25zb2xlLmxvZyhlcnIpfVxuICAgICAgXG4gICAgKTtcbiAgfVxuXG59XG4iXX0=