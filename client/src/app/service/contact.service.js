"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var environment_1 = require("../../environments/environment");
var ContactService = /** @class */ (function () {
    function ContactService(http) {
        this.http = http;
        this.url = environment_1.environment.serverUrl + '/api/contact';
    }
    ContactService.prototype.findAll = function () {
        return this.http.get(this.url);
    };
    ContactService.prototype.findByContact = function () {
        return this.http.get(this.url + "/contact");
    };
    ContactService.prototype.add = function (contact) {
        return this.http.post(this.url, contact);
    };
    ContactService.prototype.find = function (id) {
        return this.http.get("" + this.url + id);
    };
    ContactService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], ContactService);
    return ContactService;
}());
exports.ContactService = ContactService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFjdC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY29udGFjdC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBQzNDLDZDQUFrRDtBQUdsRCw4REFBMkQ7QUFLM0Q7SUFJRSx3QkFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUY1QixRQUFHLEdBQUcseUJBQVcsQ0FBQyxTQUFTLEdBQUcsY0FBYyxDQUFDO0lBRWIsQ0FBQztJQUV6QyxnQ0FBTyxHQUFQO1FBQ0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFZLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUNFLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBYSxJQUFJLENBQUMsR0FBRyxhQUFVLENBQUMsQ0FBQTtJQUN0RCxDQUFDO0lBRUQsNEJBQUcsR0FBSCxVQUFJLE9BQWlCO1FBQ3ZCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBVSxJQUFJLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCw2QkFBSSxHQUFKLFVBQUssRUFBVTtRQUNiLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBVSxLQUFHLElBQUksQ0FBQyxHQUFHLEdBQUcsRUFBSSxDQUFDLENBQUE7SUFDbkQsQ0FBQztJQXBCVSxjQUFjO1FBSDFCLGlCQUFVLENBQUM7WUFDVixVQUFVLEVBQUUsTUFBTTtTQUNuQixDQUFDO3lDQUswQixpQkFBVTtPQUp6QixjQUFjLENBcUIxQjtJQUFELHFCQUFDO0NBQUEsQUFyQkQsSUFxQkM7QUFyQlksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgQ29udGFjdCB9IGZyb20gJy4uL2VudGl0eS9jb250YWN0JztcbmltcG9ydCB7ZW52aXJvbm1lbnR9IGZyb20gJy4uLy4uL2Vudmlyb25tZW50cy9lbnZpcm9ubWVudCc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIENvbnRhY3RTZXJ2aWNlIHtcblxuICBwcml2YXRlIHVybCA9IGVudmlyb25tZW50LnNlcnZlclVybCArICcvYXBpL2NvbnRhY3QnO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XG5cbiAgZmluZEFsbCgpOiBPYnNlcnZhYmxlIDxDb250YWN0W10+e1xuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PENvbnRhY3RbXT4odGhpcy51cmwpO1xuICB9XG5cbiAgZmluZEJ5Q29udGFjdCgpOiBPYnNlcnZhYmxlIDxDb250YWN0PntcbiAgICByZXR1cm4gdGhpcy5odHRwLmdldDxDb250YWN0PihgJHt0aGlzLnVybH0vY29udGFjdGApXG4gIH1cblxuICBhZGQoY29udGFjdCA6IENvbnRhY3QpOiBPYnNlcnZhYmxlPENvbnRhY3Q+e1xucmV0dXJuIHRoaXMuaHR0cC5wb3N0PENvbnRhY3Q+KHRoaXMudXJsLCBjb250YWN0KTtcbiAgfVxuXG4gIGZpbmQoaWQ6IG51bWJlcik6IE9ic2VydmFibGU8Q29udGFjdD57XG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8Q29udGFjdD4oYCR7dGhpcy51cmx9JHtpZH1gKVxuICB9XG59XG4iXX0=