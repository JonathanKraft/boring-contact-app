import { Component, OnInit } from '@angular/core';
import { ContactService } from '../service/contact.service';
import { Contact } from '../entity/contact';

@Component({
  selector: 'ns-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  moduleId: module.id,
})
export class ContactComponent implements OnInit {
  list: Contact[];

  constructor(private service: ContactService) { }

  ngOnInit() {
    this.service.findAll().subscribe(data=> this.list = data);
  }

}
