export interface Contact {
  id?: number;
  name?: string;
  surname: string;
  phone: string;
  role?: string;
  address?: string;
  picture?: string;
  tag?:any;
}
